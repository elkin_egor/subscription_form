<?php

namespace Drupal\subscription_form_results\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\examples\Utility\DescriptionTemplateTrait;

/**
 * Controller routines for page example routes.
 */
class SubscriptionFormResultsController extends ControllerBase {

  use DescriptionTemplateTrait;

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'subscription_form_results';
  }

  public function results() {

    \Drupal::logger('my_module')->notice("results");

    date_default_timezone_set('Europe/Berlin');

    // Read rows

    $path = 'public://subscription_form_results/';
    $file_name = 'results.csv';

    $absolute_path = \Drupal::service('file_system')->realpath($path);
    $absolute_file_path = $absolute_path.'/'.$file_name;

    $rows = [];

    if (file_exists($absolute_file_path)) {

      $file = fopen($absolute_file_path, "r");

      while (!feof($file)) {

        $row = fgetcsv($file);

        if ( null != $row && count($row) != 6 || $row === false ) continue;

        $id = $row[0];
        $name = $row[1];
        $email = $row[2];
        $category = $row[3];
        $date = $row[5];

        $rows[] = [
          'id' => $id,
          'name' => $name,
          'email' => $email,
          'cat' => $category,
          'date' => date("F j, Y, g:i a", $date),
        ];
      }

      fclose($file);

    }

    // Sorting rows

    $header = [
      'name' => ['link_sort' => 'ask', 'active' => false, 'sort' => false],
      'email' => ['link_sort' => 'ask', 'active' => false, 'sort' => false],
      'date' => ['link_sort' => 'ask', 'active' => false, 'sort' => false],
    ];

    $sort_variants = ['ask', 'desk'];
    $col_variants = ['name', 'email', 'date'];

    if (isset($_GET['sort']) && isset($_GET['col']) && in_array($_GET['sort'], $sort_variants) && in_array($_GET['col'], $col_variants)) {

      $sort_col = $_GET['col'];
      $sort_sort = $_GET['sort'];

      $header[$sort_col]['active'] = true;
      $header[$sort_col]['sort'] = $sort_sort;
      $header[$sort_col]['link_sort'] = $sort_sort=='ask'?'desk':'ask';

      $query = ['sort_col' => $sort_col, 'sort' => $sort_sort];

      // Sorting rows

      switch ($sort_col) {
        case 'name':

          if ($sort_sort == 'ask') {
            usort($rows, function ($item1, $item2) {
              return strcasecmp($item1['name'], $item2['name']);
            });
          } else {
            usort($rows, function ($item1, $item2) {
              return strcasecmp($item2['name'], $item1['name']);
            });
          }
          break;
        case 'email':

          if ($sort_sort == 'ask') {
            usort($rows, function ($item1, $item2) {
              return strcasecmp($item1['email'], $item2['email']);
            });
          } else {
            usort($rows, function ($item1, $item2) {
              return strcasecmp($item2['email'], $item1['email']);
            });
          }
          break;

        case 'date':

          if ($sort_sort == 'ask') {
            // Nothing to do - default sort
          } else {
            $rows = array_reverse($rows);
          }
          break;
      }

    } else {
      $header['date']['active'] = true;
      $header['date']['sort'] = 'ask';
      $header['date']['link_sort'] = 'desk';

      $query = ['sort_col' => 'date', 'sort' => 'ask'];
    }

    //dpm($header);
    //dpm($rows);

    return [
      '#theme' => 'subscription_form_results',
      '#header' => $header,
      '#rows' => $rows,
      '#query_vars' => $query,
    ];

    return $form;
  }

  public function deleteRow($id) {

    if ($this->removeRow($id)) {
      \Drupal::messenger()->addStatus(t('The row has been deleted.'));
    } else {
      \Drupal::messenger()->addWarning(t('Can\'t remove the row'));
    }

    if ($_GET['sort'] && $_GET['col']) {
      $redirect_path = "/subscription_form_results?sort=".$_GET['sort']."&col=".$_GET['col'];
    } else {
      $redirect_path = "/subscription_form_results";
    }

    return new RedirectResponse($redirect_path);
  }

  protected function removeRow($id) {

    $path = 'public://subscription_form_results/';
    $file_name = 'results.csv';

    $absolute_path = \Drupal::service('file_system')->realpath($path);
    $absolute_file_path = $absolute_path.'/'.$file_name;

    $rows = [];

    if (file_exists($absolute_file_path)) {

      if ($file = fopen($absolute_file_path, "rw")) {

        while (!feof($file)) {

          $row = fgetcsv($file);

          if ( null != $row && count($row) != 6 || $row === false ) continue;

          if ($row[0] == $id) continue;

          $rows[] = $row;

        }

        fclose($file);

        $fp = fopen($absolute_file_path, 'w');
        foreach ($rows as $iter) {
            fputcsv($fp, $iter);
        }
        fclose($fp);

        return true;

      } else {
        return false;
      }

    } else {
      return false;
    }



  }

  public function editRow($id, $type, $value) {

    $path = 'public://subscription_form_results/';
    $file_name = 'results.csv';

    $absolute_path = \Drupal::service('file_system')->realpath($path);
    $absolute_file_path = $absolute_path.'/'.$file_name;

    $rows = [];

    if (file_exists($absolute_file_path)) {

      if ($file = fopen($absolute_file_path, "rw")) {

        while (!feof($file)) {

          $row = fgetcsv($file);

          if ( null != $row && count($row) != 6 || $row === false ) continue;

          if ($row[0] == $id) {

            switch ($type) {
              case 'name':
                $row[1] = $value;
                break;

              case 'email':
                $row[2] = $value;
                break;
            }
          }

          $rows[] = $row;

        }

        fclose($file);

        $fp = fopen($absolute_file_path, 'w');
        foreach ($rows as $iter) {
            fputcsv($fp, $iter);
        }
        fclose($fp);

        return new JsonResponse('saved');

      } else {
        return new JsonResponse('fail');
      }

    } else {
      return new JsonResponse('fail');
    }

  }
}
