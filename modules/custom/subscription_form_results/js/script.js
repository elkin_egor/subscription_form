/**
 * @file
 * JavaScript for subscription_form_results
 */

(function ($) {


  Drupal.behaviors.subscriptionFormResults = {
    attach: function () {

      $('.name_cel').once('subscriptionFormResults').on( "click", function() {
        if ($(this).text()) {
          let val = $(this).text();
          $(this).html('<form class="field_edit_form"><input name="name" type="text" value="'+val+'" /><input type="submit" value="Save" class="button"></form>');
        }
      });

      $('.email_cel').once('subscriptionFormResults').on( "click", function() {
        if ($(this).text()) {
          let val = $(this).text();
          $(this).html('<form class="field_edit_form"><input name="email" type="text" value="'+val+'" /><input type="submit" value="Save" class="button"></form>');
        }
      });

      $(document).once('subscriptionFormResults').on('submit','form.field_edit_form',function(event){

        let val = $(this).find('input').val();
        let type = $(this).find('input').attr("name");
        let id = $(this).find('input').parents(".row_item").data("id");

        console.log("/edit_row/"+id+"/"+type+"/"+val);

        $.ajax({
            url: "/edit_row/"+id+"/"+type+"/"+val,
            method :'GET',
            dataType: "json",
            success: function(result){
              if (result == 'saved') {
                $("."+id+" td."+type+"_cel").text(val);
              } else {
                alert('Can\'t save the value, please contact site admin');
              }
            },
            error: function (request, status, error) {
              alert('Can\'t save the value, please contact site admin');
            }
        });

        event.preventDefault()
      });

    }
  };

})(jQuery);
