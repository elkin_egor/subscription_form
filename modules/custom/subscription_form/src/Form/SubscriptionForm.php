<?php

namespace Drupal\subscription_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Implements the SubscriptionForm form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class SubscriptionForm extends FormBase {

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Enter your first name.'),
      '#required' => TRUE,
    ];

    // Email.
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Enter your email.'),
      '#required' => TRUE,
    ];

    $vid = 'category';
    $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
     $term_data[$term->name] = $term->name;
   }

   // Select.
   $form['category'] = [
     '#type' => 'select',
     '#title' => $this->t('Select news categories'),
     '#options' => $term_data,
     '#empty_option' => $this->t('-select-'),
     '#required' => TRUE,
     '#multiple' => TRUE,
   ];

   // Radios.
   $form['template'] = [
     '#type' => 'radios',
     '#title' => $this->t('Email template'),
     '#options' => [
       1 => $this->t('Template 1'),
       2 => $this->t('Template 2'),
       3 => $this->t('Template 3')
     ],
     '#required' => TRUE,
     '#description' => $this->t('Choose subscription email template'),
   ];


    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    $form['#attached']['library'][] = 'subscription_form/form_page';

    return $form;
  }


  public function getFormId() {
    return 'test_task_subscription_form';
  }


  public function validateForm(array &$form, FormStateInterface $form_state) {

    $name = $form_state->getValue('name');
    $email = $form_state->getValue('email');
    $category = $form_state->getValue('category');

    if ($email !== '' && !\Drupal::service('email.validator')->isValid($email)) {
      $form_state->setError('email', t('The email address %mail is not valid.', [
      '%mail' => $email]));
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

    date_default_timezone_set('Europe/Berlin');

    $name = $form_state->getValue('name');
    $email = $form_state->getValue('email');
    $category = $form_state->getValue('category');
    $template = $form_state->getValue('template');

    $path = 'public://subscription_form_results/';
    $file_name = 'results.csv';

    $absolute_path = \Drupal::service('file_system')->realpath($path);
    $absolute_file_path = $absolute_path.'/'.$file_name;

    if (\Drupal::service('file_system')->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY)) {

      $fp = fopen($absolute_file_path, 'a');

      if ($fp) {
        fputcsv($fp, [md5($name.$email.time()), $name, $email, implode(", ", $category), $template, time()]);
        fclose($fp);

        $this->messenger()->addMessage($this->t('You successfully subscribed'));
      } else {
        $this->messenger()->addMessage($this->t('Some problems, contact admin'));
      }

    }
    else {
      $this->messenger()->addMessage($this->t('Some problems, contact admin'));
    }

  }

}
